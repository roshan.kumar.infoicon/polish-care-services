package com.infoicon.polishcareservices.utils

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle

object CommonUtils {
    fun navigateToActivity(context: Context, activityClass: Class<*>, bundle: Bundle? = null) {
        val intent = Intent(context, activityClass)
        bundle?.let { intent.putExtras(it) }
        context.startActivity(intent)
        if (context is Activity) {
            context.finish()
        }
    }
}