package com.infoicon.polishcareservices.activity

import android.os.Bundle
import android.view.View
import androidx.activity.enableEdgeToEdge
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.ViewCompat
import androidx.core.view.WindowInsetsCompat
import com.infoicon.polishcareservices.R
import com.infoicon.polishcareservices.activity.adapter.BottomNavAdapter
import com.infoicon.polishcareservices.activity.model.BottomBarItem
import com.infoicon.polishcareservices.databinding.ActivityPolishCareBinding

class PolishCareActivity : AppCompatActivity() {
    private lateinit var binding: ActivityPolishCareBinding
    private lateinit var adapter: BottomNavAdapter
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        enableEdgeToEdge()
        binding = ActivityPolishCareBinding.inflate(layoutInflater)
        setContentView(binding.root)
        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.main)) { v, insets ->
            val systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars())
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom)
            insets
        }
        initBottomItem()

    }

    private fun initBottomItem() {
        val bottomBarItemList: List<BottomBarItem> by lazy {
            // This lambda expression initializes the list when first accessed
            listOf(
                BottomBarItem(0, "HOME", R.drawable.home_24px),
                BottomBarItem(1, "VISITS", R.drawable.visits),
                BottomBarItem(2, "WEEKLY", R.drawable.weekly_break_report),
                BottomBarItem(2, "SETTING", R.drawable.settings_24px)
                // Add more items as needed
            )
        }
        adapter = BottomNavAdapter(bottomBarItemList)
        binding.bottomNavigation.adapter = adapter
        adapter.setOnItemClickListener { _, item ->

        }

    }

    fun showBottombar() {
        binding.apply {
            bottomNavigation.visibility = View.VISIBLE
            view.visibility = View.VISIBLE
        }

    }

    fun hideBottomBar() {
        binding.apply {
            bottomNavigation.visibility = View.GONE
            view.visibility = View.GONE
        }

    }

    fun showToolbar() {
        binding.toolbar.visibility = View.VISIBLE
    }

    fun hideToolBar() {
        binding.toolbar.visibility = View.GONE
    }
}