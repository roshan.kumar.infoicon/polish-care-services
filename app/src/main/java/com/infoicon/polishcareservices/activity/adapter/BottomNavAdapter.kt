package com.infoicon.polishcareservices.activity.adapter

import android.graphics.PorterDuff
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.infoicon.polishcareservices.R
import com.infoicon.polishcareservices.activity.model.BottomBarItem
import com.infoicon.polishcareservices.databinding.ItemBottomBarBinding

class BottomNavAdapter(private val arrlist: List<BottomBarItem>) :
    RecyclerView.Adapter<BottomNavAdapter.ViewHolder>() {
    private var onItemClick: (Int, BottomBarItem) -> Unit = { _, _ -> }
    private var selectedPosition: Int = RecyclerView.NO_POSITION

    fun setOnItemClickListener(listener: (Int, BottomBarItem) -> Unit) {
        onItemClick = listener
    }

    inner class ViewHolder(private val binding: ItemBottomBarBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(item: BottomBarItem, isSelected: Boolean) {
            val context = binding.root.context
            binding.apply {
                imgIcon.setImageResource(item.icon)
                txtTitle.text = (item.name)
                val color = if (isSelected) {
                    ContextCompat.getColor(context, R.color.margenta)
                } else {
                    ContextCompat.getColor(context, R.color.grey)
                }

                txtTitle.setTextColor(color)
                imgIcon.setColorFilter(color, PorterDuff.Mode.SRC_IN)
                cardClick.setOnClickListener {
                    val previousSelectedPosition = selectedPosition
                    selectedPosition = adapterPosition
                    notifyItemChanged(previousSelectedPosition)
                    notifyItemChanged(selectedPosition)
                    onItemClick.invoke(adapterPosition, item)
                }
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            ItemBottomBarBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return arrlist.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(arrlist[position], position == selectedPosition)
    }
}