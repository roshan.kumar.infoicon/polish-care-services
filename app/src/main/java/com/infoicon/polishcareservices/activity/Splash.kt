package com.infoicon.polishcareservices.activity

import android.os.Build
import android.os.Bundle
import android.view.WindowInsetsController
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.ViewCompat
import androidx.core.view.WindowCompat
import androidx.core.view.WindowInsetsCompat
import androidx.lifecycle.lifecycleScope
import com.infoicon.polishcareservices.MainActivity
import com.infoicon.polishcareservices.R
import com.infoicon.polishcareservices.databinding.ActivitySplashBinding
import com.infoicon.polishcareservices.utils.CommonUtils
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch


class Splash : AppCompatActivity() {


    private lateinit var binding: ActivitySplashBinding

    @RequiresApi(Build.VERSION_CODES.R)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivitySplashBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val controller = window.insetsController
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) {
            controller?.systemBarsBehavior = WindowInsetsController.BEHAVIOR_DEFAULT
        }
        WindowCompat.setDecorFitsSystemWindows(window, false)
        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.main)) { v, insets ->
            val systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars())
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom)
            insets
        }

        goToActivity()

    }

    private fun goToActivity() {
        lifecycleScope.launch(Dispatchers.Main) {
            delay(2000)
            CommonUtils.navigateToActivity(this@Splash, PolishCareActivity::class.java)
        }
    }


}