package com.infoicon.polishcareservices.activity.model

data class BottomBarItem(
    val id: Int,
    val name: String,
    val icon: Int
)