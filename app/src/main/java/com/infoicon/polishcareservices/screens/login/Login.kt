package com.infoicon.polishcareservices.screens.login

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.infoicon.polishcareservices.R
import com.infoicon.polishcareservices.databinding.FragmentLoginBinding


class Login : Fragment(R.layout.fragment_login) {

    private lateinit var binding: FragmentLoginBinding

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = FragmentLoginBinding.bind(view)
        initListeners()

    }

    private fun initListeners() {
        binding.apply {
            txtForgetPassword.setOnClickListener {
                findNavController().navigate(R.id.action_login_to_password)
            }
        }
    }


}