package com.infoicon.polishcareservices.screens.forgetpassword

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.infoicon.polishcareservices.R
import com.infoicon.polishcareservices.databinding.FragmentPasswordBinding


class Password : Fragment(R.layout.fragment_password) {

    private lateinit var binding: FragmentPasswordBinding
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentPasswordBinding.inflate(layoutInflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initSetview()
        initListeners()
    }

    private fun initSetview() {
        binding.apply {
            btnLogin.txtItem.text = "SEND"
        }
    }

    private fun initListeners() {
        binding.btnLogin.btnClick.setOnClickListener {

        }
    }

}