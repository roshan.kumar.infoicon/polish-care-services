package com.infoicon.polishcareservices.di

import android.content.Context
import com.infoicon.polishcareservices.network.Keys
import com.infoicon.polishcareservices.network.ApiService
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Named


@Module
@InstallIn(SingletonComponent::class)
object ApplicationModule {

    @Provides
    fun provideOkhttpClient(@ApplicationContext context: Context): OkHttpClient {
        val httpLoggingInterceptor = HttpLoggingInterceptor()
        httpLoggingInterceptor.level = HttpLoggingInterceptor.Level.BODY
        return OkHttpClient.Builder().addInterceptor(httpLoggingInterceptor)
            .connectTimeout(30 * 60 * 1000L, TimeUnit.SECONDS)
            .writeTimeout(30 * 60 * 1000L, TimeUnit.SECONDS)
            .readTimeout(30 * 60 * 1000L, TimeUnit.SECONDS).build()

    }

    @Provides
    @Named("BaseApi")
    fun provideMoneyRetrofit(okHttpClient: OkHttpClient): Retrofit = Retrofit
        .Builder().baseUrl(Keys.BASEURL)
        .addConverterFactory(GsonConverterFactory.create())
        .client(okHttpClient)
        .build()

    @Provides
    fun provideMoneyService(@Named("BaseApi") retrofit: Retrofit): ApiService =
        retrofit.create(ApiService::class.java)
    /*
        @Provides
        @Singleton
        fun provideAppDatabase(@ApplicationContext context: Context): AppDatabase {
            return Room.databaseBuilder(context, AppDatabase::class.java, "user_db").allowMainThreadQueries().build()

        }

        @Provides
        fun provideUserDao(database: AppDatabase): UserDao {
            return database.userDao()
        }*/


}