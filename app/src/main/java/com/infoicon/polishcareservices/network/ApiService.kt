package com.infoicon.polishcareservices.network

import com.infoicon.polishcareservices.model.LoginResponse
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.POST

interface ApiService{
    @FormUrlEncoded
    @POST("login")
    suspend fun doLogin(
        @Field("username") name: String,
        @Field("password") pass: String,
    ): LoginResponse

}


