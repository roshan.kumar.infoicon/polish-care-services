package com.infoicon.polishcareservices.network

enum class Status {
    Success,
    Loading,
    Failure
}