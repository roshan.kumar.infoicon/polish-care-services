package com.infoicon.polishcareservices.repository

import com.infoicon.polishcareservices.network.ApiService
import javax.inject.Inject

class MainRepository @Inject constructor(private val apiService: ApiService) {
    suspend fun login(name: String, pass: String) =
        apiService.doLogin(name, pass)
}