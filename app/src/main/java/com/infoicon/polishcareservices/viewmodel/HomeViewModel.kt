package com.infoicon.polishcareservices.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.liveData
import com.infoicon.polishcareservices.network.Resource
import com.infoicon.polishcareservices.repository.MainRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import javax.inject.Inject

@HiltViewModel
class HomeViewModel @Inject constructor(private val mainRepository: MainRepository) : ViewModel() {

    fun login(name: String, pass: String) = liveData(Dispatchers.IO) {
        emit(Resource.loading(data = null))
        try {
            emit(
                Resource.success(
                    data = mainRepository.login(name, pass)
                )
            )
        } catch (ex: Exception) {
            emit(Resource.error(data = null, message = ex.message ?: "Error Occurred!"))
        }
    }
}